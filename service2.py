import requests
import sys

# here should be url where service1 is running
SERVICE1_URL = "http://159.223.23.111/"

print("Enter the web page URL:")
message = requests.get(sys.stdin.readline()).text
data = ["md5", message]

# sending post request on service1_url with data = "md5\n url"
print(requests.post(SERVICE1_URL, data="\n".join(data)))

